$(window).on("load", function(){
    $('.loading').fadeOut();
});


// $('img').load(function(){
//     if($(this).height() > 10) {
//         $('.loading').fadeOut();
//     }
// });

$("#flipEntry").flip({trigger:'manual', speed:350});

var textContent = $('textarea').val();

function checkInput() {
    setInterval(function(){ 
        textContent = $('textarea').val();
        if (textContent) {
            $('.placeholder-container').hide()
            $('.confirm').css({opacity: 1});
        } else {
            $('.placeholder-container').show()
            $('.confirm').css({opacity: 0});
        }
    }, 30);
}

checkInput();

function myFunction() {
  var x = document.getElementById('.myInput').value;
  document.getElementById('.btn-animated').innerHTML = "You wrote: " + x;
}

var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        // debugger;
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };


//// Terrible hacky code

    function initializeTypewrite2(txt) {
        // debugger;
        // var texty = txt;
        console.log('ehllo')
        var elements = document.getElementsByClassName('typewrite2');
        for (var i=0; i<elements.length; i++) {
            // var toRotate = texty;
            var period = elements[i].getAttribute('data-period');
            if (txt) {
                new TxtType2(elements[i], JSON.parse(txt), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite2 > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };

    var TxtType2 = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType2.prototype.tick = function() {
        // debugger
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

/// End of terrible hacky code

 var nameList= ['mark','donna','elana','steve'];

 $('.confirm').click(
    function(e){
        if (textContent) {
        e.preventDefault();
        $("#flipEntry").flip(true);
        showCustomPage(textContent)
     }
    }
);


function showCustomPage(name) {
    name = name.toLowerCase();

    console.log(nameObj[name].text)
    initializeTypewrite2(nameObj[name].text)
    $("#personalEntryPic").attr("src",nameObj[name].image);
}

var nameObj = {
    katie: {
        text: `["Hi shmatie I can't wait to see your face!!"]`,
        image: "../images/name/katie/0.jpg"
    },
    kathryn: {
        text: `["Hi shmatie I can't wait to see your face!!"]`,
        image: "../images/name/katie/0.jpg"
    },
    donna: {
        text: `["Hi mommala see you soon roomie!!"]`,
        image: "../images/name/donna/0.jpg"
    },
    mom: {
        text: `["Hi mommala see you soon roomie!!"]`,
        image: "../images/name/donna/0.jpg"
    },
    adam: {
        text: `["Hi hazza I miss you already!!"]`,
        image: "../images/name/adam/0.jpg"
    },
    hazza: {
        text: `["Hi hazza I miss you already!!"]`,
        image: "../images/name/adam/0.jpg"
    },
    bec: {
        text: `["Hi beccy I miss you already!!]`,
        image: "../images/name/bec/0.jpg"
    },
    rebecca: {
        text: `["Hi beccy I miss you already!!]`,
        image: "../images/name/bec/0.jpg"
    },
    brett: {
        text: `["Hi hbr I miss you already!!"]`,
        image: "../images/name/brett/0.jpg"
    },
    calvin: {
        text: `["Hi cal I miss you already my sista"]`,
        image: "../images/name/calvin/0.jpg"
    },
    elana: {
        text: `["Hi musky you're my best friend in the world"]`,
        image: "../images/name/elana/0.jpg"
    },
    haley: {
        text: `["Hi sis see you soon neighbor!!"]`,
        image: "../images/name/haley/0.jpg"
    },
    ian: {
        text: `["Hi haze I miss you already!!"]`,
        image: "../images/name/ian/0.jpg"
    },
    hazel: {
        text: `["Hi haze I miss you already!!"]`,
        image: "../images/name/ian/0.jpg"
    },
    haze: {
        text: `["Hi haze I miss you already!!"]`,
        image: "../images/name/ian/0.jpg"
    },
    jason: {
        text: `["Hi silv I miss you already!!"]`,
        image: "../images/name/jason/0.jpg"
    },
    silver: {
        text: `["Hi silv I miss you already!!"]`,
        image: "../images/name/jason/0.jpg"
    },
    mark: {
        text: `["Hi daddala see you soon roomie!!"]`,
        image: "../images/name/mark/0.jpg"
    },
    dad: {
        text: `["Hi daddala see you soon roomie!!"]`,
        image: "../images/name/mark/0.jpg"
    },
    mikey: {
        text: `["Hi mikey b nyc see you soon best pal!!"]`,
        image: "../images/name/mikey/0.jpg"
    },
    mike: {
        text: `["Hi mikey b nyc see you soon best pal!!"]`,
        image: "../images/name/mikey/0.jpg"
    },
    michael: {
        text: `["Hi mikey b nyc see you soon best pal!!"]`,
        image: "../images/name/mikey/0.jpg"
    },
    mila: {
        text: `["Hi my sister I love you forever and ever"]`,
        image: "../images/name/mila/0.jpg"
    },
    steven: {
        text: `["Hi stevie I miss you already!!"]`,
        image: "../images/name/steve/0.jpg"
    },
    steve: {
        text: `["Hi stevie I miss you already!!"]`,
        image: "../images/name/steve/0.jpg"
    },
    filler: {
        text: `["Hi stevie I miss you already!!"]`,
        image: "../images/name/steve/0.jpg"
    },
    fila: {
        text: `["Hi stevie I miss you already!!"]`,
        image: "../images/name/steve/0.jpg"
    },
    stevie: {
        text: `["Hi stevie I miss you already!!"]`,
        image: "../images/name/steve/0.jpg"
    },
    zach: {
        text: `["Hi pookie see you soon roomie!!"]`,
        image: "../images/name/zach/0.jpg"
    },
    zachary: {
        text: `["Hi pookie see you soon roomie!!"]`,
        image: "../images/name/zach/0.jpg"
    },
    jamie: {
        text: `["Hi jaminé I can't wait to see your face!!"]`,
        image: "../images/name/jamie/0.jpg"
    },
    jordan: {
        text: `["Hi goober wanna move to new york together?!?"]`,
        image: "../images/name/jordan/0.jpg"
    },
    gruber: {
        text: `["Hi goober wanna move to new york together?!?"]`,
        image: "../images/name/jordan/0.jpg"
    },
    josh: {
        text: `["Hi my joj!! lil dots live in squeeze city!!!"]`,
        image: "../images/name/josh/0.jpg"
    },
    joj: {
        text: `["Hi my joj!! lil dots live in squeeze city!!!"]`,
        image: "../images/name/josh/0.jpg"
    },
    joshua: {
        text: `["Hi my joj!! lil dots live in squeeze city!!!"]`,
        image: "../images/name/josh/0.jpg"
    },
    maddi: {
        text: `["Hi wad I can't wait to see your face!!"]`,
        image: "../images/name/maddi/0.jpg"
    },
    madeleine: {
        text: `["Hi wad I can't wait to see your face!!"]`,
        image: "../images/name/maddi/0.jpg"
    },
    mariel: {
        text: `["Hi murhla I can't wait to see your face!!"]`,
        image: "../images/name/mariel/0.jpg"
    },
    volpert: {
        text: `["Hi shmolpy I can't wait to see your face!!"]`,
        image: "../images/name/volp/0.jpg"
    },
    volp: {
        text: `["Hi shmolpy I can't wait to see your face!!"]`,
        image: "../images/name/volp/0.jpg"
    },
    oliver: {
        text: `["Hi bogust I miss you already!!"]`,
        image: "../images/name/oliver/0.jpg"
    },
    bog: {
        text: `["Hi bogust I miss you already!!"]`,
        image: "../images/name/oliver/0.jpg"
    },
    nrob: {
        text: `["Hi enny I can't wait to see your face!!"]`,
        image: "../images/name/nrob/0.jpg"
    },
    enny: {
        text: `["Hi enny I can't wait to see your face!!"]`,
        image: "../images/name/nrob/0.jpg"
    },
    valner: {
        text: `["Hi val I can't wait to see your face!!"]`,
        image: "../images/name/val/0.jpg"
    },
    niki: {
        text: `["Hi val I can't wait to see your face!!"]`,
        image: "../images/name/val/0.jpg"
    },
    val: {
        text: `["Hi val I can't wait to see your face!!"]`,
        image: "../images/name/val/0.jpg"
    },
    sophia: {
        text: `["Hi my dina I can't wait to see your face!!"]`,
        image: "../images/name/sophia/0.jpg"
    },
    dina: {
        text: `["Hi my dina I can't wait to see your face!!"]`,
        image: "../images/name/sophia/0.jpg"
    },
    pari: {
        text: `["Hi pam I miss you already!!"]`,
        image: "../images/name/pari/0.jpg"
    },
    pam: {
        text: `["Hi pam I miss you already!!"]`,
        image: "../images/name/pari/0.jpg"
    },
    roy: {
        text: `["Hi my israeli I miss you so much!!"]`,
        image: "../images/name/roy/0.jpg"
    },
    mikayla: {
        text: `["Hi my mikika I miss you so much!!"]`,
        image: "../images/name/mikayla/0.jpg"
    },
};